import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.regex.Pattern.CASE_INSENSITIVE;

public class UserMap extends LinkedHashMap<Integer, User> implements CoolInterface {

    public LinkedHashMap<Integer, User> getUserMap() {
        return this;
    }

    @Override
    public void addUser(User user) {
        put(user.getId(), user);
    }

    @Override
    public void removeUser(User user) {
        remove(user.getId());
    }

    @Override
    public User getMaxUserById() {
        return Collections.max(this.values(), User.getIdComparator());
    }

    @Override
    public User getMinUserById() {
        return Collections.min(this.values(), User.getIdComparator());
    }

    @Override
    public void deleteSmallerValues(int idUser) {
        List<User> users = new ArrayList<>();
        for (User user : this.values()) {
            if (user.getId() < idUser)
                users.add(user);
        }
        for (User user : users) {
            removeUser(user);
        }
    }

    @Override
    public void deleteGreaterValues(int idUser) {
        List<User> users = new ArrayList<>();
        for (User user : this.values()) {
            if (user.getId() > idUser)
                users.add(user);
        }
        for (User user : users) {
            removeUser(user);
        }
    }

    @Override
    public long getSumId() {
        long sumId = 0;
        for (User user : this.values()) {
            sumId += user.getId();
        }
        return sumId;
    }

    @Override
    public User getUser(int index) {
        int indexInList = 0;
        for (User user : this.values()) {
            if (indexInList == index) {
                return user;
            }
            indexInList += 1;
        }
        return null;
    }

    @Override
    public List<User> getSomeUsers(int index, int numberUsers) {
        LinkedList<User> users = new LinkedList<>();
        for (int i = index; i >= index && i < index + numberUsers; ++i) {
            users.add(this.getUser(i));
        }
        return users;
    }

    @Override
    public void updateAllNameUsers(String text) {
        for (User user : this.values()) {
            user.setName(user.getName().concat(text));
        }
    }

    @Override
    public List sortUsersById() {
        List<User> users = new LinkedList<>();
        users.addAll(this.values());
        users.sort(User.getIdComparator());
        return users;
    }

    @Override
    public List sortUserByName() {
        List<User> users = new LinkedList<>();
        users.addAll(this.values());
        users.sort(User.getNameComparator());
        return users;
    }

    @Override
    public List<User> searchUsersByName(String characters) {
        LinkedList<User> users = new LinkedList<>();
        for (User user : this.values()) {
            if (checkCharactersInText(user.getName(), characters)) {
                users.add(user);
            }
        }
        return users;
    }

    @Override
    public void print() {
        for (User user : this.values()) {
            System.out.println(user.toString());
        }
    }

    @Override
    public List<User> getUserGreaterIds(int idUser) {
        List<User> users = new ArrayList<>();
        for (User user : this.values()) {
            if (user.getId() >= idUser)
                users.add(user);
        }
        return users;
    }


    private boolean checkCharactersInText(String text, String characters) {
        Pattern p = Pattern.compile(".*" + characters + ".*", CASE_INSENSITIVE);
        Matcher m = p.matcher(text);
        return m.matches();
    }
}
