import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.util.regex.Pattern.CASE_INSENSITIVE;

public class UserMapStreamAPI implements CoolInterface {
    private Map<Integer, User> users = new LinkedHashMap<>();

    @Override
    public void addUser(User user) {
        users.put(user.getId(), user);
    }

    @Override
    public void removeUser(User user) {
        users.remove(user.getId());
    }

    @Override
    public User getMaxUserById() {
        Optional<User> user = users.values().stream().max(User.getIdComparator());
        return user.get();
    }

    @Override
    public User getMinUserById() {
        Optional<User> user = users.values().stream().min(User.getIdComparator());
        return user.get();
    }

    @Override
    public void deleteSmallerValues(int idUser) {
        users = users.values().stream().filter(u -> u.getId() >= idUser)
                .collect(Collectors.toMap(User::getId, user -> user));
    }

    @Override
    public void deleteGreaterValues(int idUser) {
        users = users.values().stream().filter(u -> u.getId() <= idUser)
                .collect(Collectors.toMap(User::getId, user -> user));
    }

    @Override
    public long getSumId() {
        return users.values().stream().mapToInt(User::getId).sum();
    }

    @Override
    public User getUser(int index) {
        Optional<User> user = users.values().stream().skip(index).findFirst();
        return user.get();
    }

    @Override
    public List<User> getSomeUsers(int index, int numberUsers) {
        return users.values().stream().skip(index).limit(numberUsers).collect(Collectors.toList());
    }

    @Override
    public void updateAllNameUsers(String text) {
        users.values().forEach((u) -> u.setName(u.getName() + text));
    }

    @Override
    public List<User> searchUsersByName(String characters) {
        return users.values().stream().filter(u -> checkCharactersInText(u.getName(), characters))
                .collect(Collectors.toList());
    }

    @Override
    public void print() {
        for (User user : users.values()) {
            System.out.println(user.toString());
        }
    }

    @Override
    public List<User> getUserGreaterIds(int idUser) {
        return users.values().stream().filter(u -> u.getId() >= idUser).collect(Collectors.toList());
    }

    @Override
    public List sortUsersById() {
        return users.values().stream().sorted(User.getIdComparator()).collect(Collectors.toList());
    }

    @Override
    public List sortUserByName() {
        return users.values().stream().sorted(User.getNameComparator()).collect(Collectors.toList());
    }

    private boolean checkCharactersInText(String text, String characters) {
        Pattern p = Pattern.compile(".*" + characters + ".*", CASE_INSENSITIVE);
        Matcher m = p.matcher(text);
        return m.matches();
    }
}
