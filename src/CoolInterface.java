import java.util.List;

public interface CoolInterface {
    void addUser(User user);

    void removeUser(User user);

    User getMaxUserById();

    User getMinUserById();

    void deleteSmallerValues(int idUser);

    void deleteGreaterValues(int idUser);

    long getSumId();

    User getUser(int index);

    List<User> getSomeUsers(int index, int numberUsers);

    void updateAllNameUsers(String text);

    List<User> searchUsersByName(String characters);

    void print();

    List<User> getUserGreaterIds(int idUser);

    List sortUsersById();

    List sortUserByName();
}
