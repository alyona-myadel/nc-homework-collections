import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.util.regex.Pattern.CASE_INSENSITIVE;

public class UserListStreamAPI implements CoolInterface {
    private List<User> users = new LinkedList<>();

    @Override
    public void addUser(User user) {
        users.add(user);
    }

    @Override
    public void removeUser(User user) {
        users = users.stream().filter(u -> u.getId() != user.getId() && !u.getName().equals(user.getName()))
                .collect(Collectors.toList());
    }

    @Override
    public User getMaxUserById() {
        Optional<User> user = users.stream().max(User.getIdComparator());
        return user.get();
    }

    @Override
    public User getMinUserById() {
        Optional<User> user = users.stream().min(User.getIdComparator());
        return user.get();
    }

    @Override
    public void deleteSmallerValues(int idUser) {
        users = users.stream().filter(u -> u.getId() >= idUser).collect(Collectors.toList());
    }

    @Override
    public void deleteGreaterValues(int idUser) {
        users = users.stream().filter(u -> u.getId() <= idUser).collect(Collectors.toList());
    }

    @Override
    public long getSumId() {
        return users.stream().mapToInt(User::getId).sum();
    }

    @Override
    public User getUser(int index) {
        Optional<User> user = users.stream().skip(index).findFirst();
        return user.get();
    }

    @Override
    public List<User> getSomeUsers(int index, int numberUsers) {
        return users.stream().skip(index).limit(numberUsers).collect(Collectors.toList());
    }

    @Override
    public void updateAllNameUsers(String text) {
        users.forEach((u) -> u.setName(u.getName() + text));
    }

    @Override
    public List<User> searchUsersByName(String characters) {
        return users.stream().filter(u -> checkCharactersInText(u.getName(), characters)).collect(Collectors.toList());
    }

    @Override
    public void print() {
        for (User user : users) {
            System.out.println(user.toString());
        }
    }

    @Override
    public List<User> sortUsersById() {
        return users.stream().sorted(User.getIdComparator()).collect(Collectors.toList());
    }

    @Override
    public List<User> sortUserByName() {
        return users.stream().sorted(User.getNameComparator()).collect(Collectors.toList());
    }

    public List<User> getUserGreaterIds(int idUser) {
        return users.stream().filter(u -> u.getId() >= idUser).collect(Collectors.toList());
    }

    private boolean checkCharactersInText(String text, String characters) {
        Pattern p = Pattern.compile(".*" + characters + ".*", CASE_INSENSITIVE);
        Matcher m = p.matcher(text);
        return m.matches();
    }
}
